# @openstapps/core

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/core.svg?style=flat-square)](https://gitlab.com/openstapps/core/commits/master) 
[![npm](https://img.shields.io/npm/v/@openstapps/core.svg?style=flat-square)](https://npmjs.com/package/@openstapps/core)
[![license)](https://img.shields.io/npm/l/@openstapps/core.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/core)

StAppsCore - Generalized model of data

## What is the StAppsCore?

The StAppsCore (SC) is a generalized model of data.

SC is based on the idea of [schema.org](https://www.schema.org). The SC structures data by describing objects as so called SC-types like: Event, Place, Date, Person, ...

Data sources of the same type of data are assigned to the same SC-type.

### Requirements for the SC

* Study processes which can be generalized are abstracted and modeled in that manner that the structure can be adapted to any german university.
* It must be platform- and software-independent.
* Search interface must be clearly specified that different working groups with different search tools can retrieve the same search results.
* Must be expandable.

### Generate documentation for routes

To generate a documentation for the routes use the following command.

```shell
node --require ts-node/register src/cli.ts routes PATH/TO/ROUTES.md
```
