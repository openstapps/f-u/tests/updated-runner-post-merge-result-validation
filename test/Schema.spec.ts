/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {validateFiles, writeReport} from '@openstapps/core-tools/lib/validate';
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {mkdirSync} from 'fs';
import {slow, suite, test, timeout} from 'mocha-typescript';
import {join, resolve} from 'path';

const logger = new Logger();

process.on('unhandledRejection', (err) => {
  logger.error('UNHANDLED REJECTION', err.stack);
  process.exit(1);
});

@suite(timeout(10000), slow(5000))
export class ValidateTestFiles {
  @test
  async validateTestFiles() {
    const errorsPerFile = await validateFiles(resolve('lib', 'schema'), resolve('test', 'resources'));

    let unexpected = false;
    Object.keys(errorsPerFile).forEach((file) => {
      unexpected = unexpected || errorsPerFile[file].some((error) => !error.expected);
    });

    expect(unexpected).to.be.equal(false);

    mkdirSync('report', {
      recursive: true,
    });

    await writeReport(join('report', 'index.html'), errorsPerFile);
  }
}
