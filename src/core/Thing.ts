/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCOrganization} from './things/Organization';
import {SCPerson} from './things/Person';
import {SCMetaTranslations, SCTranslations} from './types/i18n';
import {SCISO8601Date} from './types/Time';
import {SCUuid} from './types/UUID';

/**
 * Types a thing can be
 */
export enum SCThingType {
  AcademicEvent = 'academic event',
  Article = 'article',
  Book = 'book',
  Building = 'building',
  Catalog = 'catalog',
  CourseOfStudies = 'course of studies',
  DateSeries = 'date series',
  Diff = 'diff',
  Dish = 'dish',
  Favorite = 'favorite',
  Floor = 'floor',
  Message = 'message',
  Organization = 'organization',
  Person = 'person',
  PointOfInterest = 'point of interest',
  Room = 'room',
  Semester = 'semester',
  Setting = 'setting',
  SportCourse = 'sport course',
  StudyModule = 'study module',
  Ticket = 'ticket',
  ToDo = 'todo',
  Tour = 'tour',
  Video = 'video',
}

/**
 * A thing
 */
export interface SCThing {
  /**
   * Alternate names of the thing
   */
  alternateNames?: string[];
  /**
   * Description of the thing
   *
   * @minLength 1
   */
  description?: string;
  /**
   * Image of the thing
   */
  image?: string;
  /**
   * Name of the thing
   *
   * @minLength 1
   */
  name: string;
  /**
   * Origin of the thing
   */
  origin: SCThingRemoteOrigin | SCThingUserOrigin;
  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingTranslatableProperties>;
  /**
   * Type of the thing
   */
  type: SCThingType;
  /**
   * Universally unique identifier of the thing
   */
  uid: SCUuid;
  /**
   * URL of the thing
   */
  url?: string;
}

/**
 * Possible types of an origin
 */
export enum SCThingOriginType {
  Remote = 'remote',
  User = 'user',
}

/**
 * Origin of a thing
 */
export interface SCThingOrigin {
  /**
   * Maintainer of the origin
   *
   * e.g. restaurant of a dish
   */
  maintainer?: SCPerson | SCOrganization;

  /**
   * When the thing was modified last in the origin
   */
  modified?: SCISO8601Date;

  /**
   * Type of the origin
   */
  type: SCThingOriginType;
}

/**
 * Remote origin of a thing
 */
export interface SCThingRemoteOrigin extends SCThingOrigin {
  /**
   * When the thing was indexed last from the origin
   */
  indexed: SCISO8601Date;

  /**
   * Name of the origin
   */
  name: string;

  /**
   * Original ID of the thing in the origin
   */
  originalId?: string;

  /**
   * Entity that is responsible for the entity
   *
   * e.g. an organizer for an event
   */
  responsibleEntity?: SCPerson | SCOrganization;

  /**
   * Type of the origin
   */
  type: SCThingOriginType.Remote;

  /**
   * Main URL of the origin
   */
  url?: string;
}

/**
 * User origin of a thing (data created through user interaction)
 */
export interface SCThingUserOrigin extends SCThingOrigin {
  /**
   * When the thing was created
   */
  created: SCISO8601Date;

  /**
   * If it is deleted or not, defaults to false
   */
  deleted?: boolean;

  /**
   * Type of the origin
   */
  type: SCThingOriginType.User;

  /**
   * When the saved thing was last updated with the latest state (e.g. from the backend)
   */
  updated?: SCISO8601Date;
}

/**
 * Translatable properties of things
 */
export interface SCThingTranslatableProperties {
  /**
   * Translation of the description of the thing
   */
  description?: string;
  /**
   * Translation of the name of the thing
   */
  name?: string;
  /**
   * Origin of the thing
   */
  origin?: SCThingTranslatablePropertyOrigin;
}

/**
 * Translatable property of an origin
 */
export interface SCThingTranslatablePropertyOrigin {
  /**
   * Translation of the name of the origin
   */
  name: string;
}

/**
 * Meta information about things
 */
export class SCThingMeta implements SCMetaTranslations<SCThing> {
  /**
   * Set type definiton for singleton instance
   */
  protected static _instance = new Map<string, unknown>();

  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      alternateNames: 'alternative Namen',
      description: 'Beschreibung',
      image: 'Bild',
      name: 'Name',
      origin: 'Ursprung',
      translations: 'Übersetzungen',
      type: 'Typ',
      uid: 'Identifikation',
      url: 'URL',
    },
    en: {
      alternateNames: 'alternate names',
      description: 'description',
      image: 'image',
      name: 'name',
      origin: 'origin',
      translations: 'translations',
      type: 'type',
      uid: 'identification',
      url: 'URL',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      type: 'Ding',
    },
    en: {
      type: 'Thing',
    },
  };

  /**
   * Function to retrieve typed singleton instance
   */
  public static getInstance<T extends SCThingMeta>(): T {
    if (!this._instance.has(this.name)) {
      this._instance.set(this.name, new this());
    }
    return this._instance.get(this.name) as T;
  }

  protected constructor() {}
}
