/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';

/**
 * An academic term without references
 */
export interface SCAcademicTermWithoutReferences extends SCThing {
  /**
   * Short name of the academic term, using the given pattern
   */
  acronym: string;

  /**
   * End date of the academic term
   */
  endDate: SCISO8601Date;

  /**
   * End date of lectures in the academic term
   */
  eventsEndDate?: SCISO8601Date;

  /**
   * Start date of lectures in the academic term
   */
  eventsStartDate?: SCISO8601Date;

  /**
   * Start date of the academic term
   */
  startDate: SCISO8601Date;
}

/**
 * Meta information about academic terms
 */
export class SCAcademicTermWithoutReferencesMeta extends SCThingMeta implements SCMetaTranslations<SCThing> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
