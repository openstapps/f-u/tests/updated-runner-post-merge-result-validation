/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta} from '../Thing';
import {SCCatalogWithoutReferences} from '../things/Catalog';
import {SCPersonWithoutReferences} from '../things/Person';
import {SCSemesterWithoutReferences} from '../things/Semester';
import {SCMetaTranslations} from '../types/i18n';
import {SCCreativeWorkWithoutReferences} from './CreativeWork';

/**
 * An event without references
 */
export interface SCEventWithoutReferences extends SCThing {
  /**
   * Maximum number of participants of the event
   *
   * A maximum number of people that can participate in the event.
   */
  maximumParticipants?: number;

  /**
   * Remaining attendee capacity of the event
   *
   * This number represents the remaining open spots.
   */
  remainingAttendeeCapacity?: number;
}

/**
 * An event
 */
export interface SCEvent extends SCEventWithoutReferences {
  /**
   * Academic terms that an event belongs to, e.g. semester(s).
   */
  academicTerms?: SCSemesterWithoutReferences[];

  /**
   * Catalogs to which an event belongs
   */
  catalogs?: SCCatalogWithoutReferences[];

  /**
   * A list of creative works that are associated with this event
   *
   * This can be recommended books, CDs that can be bought, etc.
   */
  creativeWorks?: SCCreativeWorkWithoutReferences[];

  /**
   * Organizers of the event
   */
  organizers?: SCPersonWithoutReferences[];

  /**
   * Performers of the event
   */
  performers?: SCPersonWithoutReferences[];
}

/**
 * Meta information about events
 */
export class SCEventMeta extends SCThingMeta implements SCMetaTranslations<SCEvent> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
