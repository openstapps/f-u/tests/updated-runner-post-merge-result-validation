/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta, SCThingTranslatableProperties} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCMap} from '../types/Map';

/**
 * A thing without references with categories
 *
 * !!! BEWARE !!!
 * `T` should be a union type - e.g. `T = 'foo' | 'bar' | 'foobar';`
 */
export interface SCThingWithCategoriesWithoutReferences<T,
  U extends SCThingWithCategoriesSpecificValues>
  extends SCThing {
  /**
   * Categories of a thing with categories
   */
  categories: T[];

  /**
   * Use this to explicitly override general opening hours brought in scope by openingHoursSpecification or openingHours
   *
   * A map from categories to their specific values.
   */
  categorySpecificValues?: SCMap<U>;

  /**
   * Translated fields of a thing with categories
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;
}

/**
 * Translatable properties of a thing with categories
 */
export interface SCThingWithCategoriesTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * translations of the categories of a thing with categories
   */
  categories?: string[];
}

/**
 * Category specific values of a thing with categories
 *
 * This interface contains properties that can be specific to a certain category.
 */
export interface SCThingWithCategoriesSpecificValues {
  /**
   * Category specific alternate names of a thing
   */
  alternateNames?: string[];

  /**
   * Category specific description of a thing
   */
  description?: string;

  /**
   * Category specific image of a thing
   */
  image?: string;

  /**
   * Category specific name of a thing
   */
  name?: string;

  /**
   * Category specific URL of a thing
   */
  url?: string;
}

/**
 * Meta information about a thing without references that accepts payments
 */
export class SCThingWithCategoriesWithoutReferencesMeta<T, U> implements
             SCMetaTranslations<SCThingWithCategoriesWithoutReferences<T, U>> {

  protected static _instance: SCThingMeta;

  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };

  /**
   * Function to retrieve typed singleton instance (including generics)
   */
  public static getInstance<T, U>(): SCThingWithCategoriesWithoutReferencesMeta<T, U> {
    return this._instance || (this._instance = new this<T, U>());
  }

  protected constructor() {
  }
}
