/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Possible HTTP verbs for routes
 */
import {SCErrorResponse} from './protocol/errors/ErrorResponse';
import {SCBookAvailabilityRequest} from './protocol/routes/bookAvailability/BookAvailabilityRequest';
import {SCBookAvailabilityResponse} from './protocol/routes/bookAvailability/BookAvailabilityResponse';
import {SCBulkRequest} from './protocol/routes/bulk/BulkRequest';
import {SCBulkResponse} from './protocol/routes/bulk/BulkResponse';
import {SCBulkAddRequest} from './protocol/routes/bulk/UID/BulkAddRequest';
import {SCBulkAddResponse} from './protocol/routes/bulk/UID/BulkAddResponse';
import {SCBulkDoneRequest} from './protocol/routes/bulk/UID/BulkDoneRequest';
import {SCBulkDoneResponse} from './protocol/routes/bulk/UID/BulkDoneResponse';
import {SCFeedbackRequest} from './protocol/routes/feedback/FeedbackRequest';
import {SCFeedbackResponse} from './protocol/routes/feedback/FeedbackResponse';
import {SCIndexRequest} from './protocol/routes/INDEX/IndexRequest';
import {SCIndexResponse} from './protocol/routes/INDEX/IndexResponse';
import {SCMultiSearchRequest} from './protocol/routes/search/MultiSearchRequest';
import {SCMultiSearchResponse} from './protocol/routes/search/MultiSearchResponse';
import {SCSearchRequest} from './protocol/routes/search/SearchRequest';
import {SCSearchResponse} from './protocol/routes/search/SearchResponse';
import {SCThingUpdateRequest} from './protocol/routes/TYPE/UID/ThingUpdateRequest';
import {SCThingUpdateResponse} from './protocol/routes/TYPE/UID/ThingUpdateResponse';
import {SCMap} from './types/Map';

/**
 * Possible Verbs for HTTP requests
 */
export enum SCRouteHttpVerbs {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
}

/**
 * The constructor of an error response
 */
export type SCErrorResponseConstructor = new (...args: any) => SCErrorResponse;

/**
 * A description of a route
 */
export interface SCRoute {
  /**
   * A map of names of possible errors that can be returned by the route with their appropriate status codes
   */
  errorNames: SCErrorResponseConstructor[];

  /**
   * HTTP verb to use to request the route
   */
  method: SCRouteHttpVerbs;

  /**
   * Map of obligatory parameters and their type that have to be set via the requested path
   */
  obligatoryParameters?: SCMap<string>;

  /**
   * Name of the type of the request body
   */
  requestBodyName: string;

  /**
   * Name of the type of the response body
   */
  responseBodyName: string;

  /**
   * Status code for success
   */
  statusCodeSuccess: number;

  /**
   * URL fragment of the route
   */
  urlFragment: string;
}

/**
 * An abstract route
 */
export abstract class SCAbstractRoute implements SCRoute {
  errorNames: SCErrorResponseConstructor[] = [];
  method: SCRouteHttpVerbs = SCRouteHttpVerbs.GET;
  obligatoryParameters?: SCMap<string>;
  requestBodyName = 'any';
  responseBodyName = 'any';
  statusCodeSuccess = 200;
  urlFragment = '/';

  public getUrlFragment(parameters?: SCMap<string>): string {
    if (typeof parameters === 'undefined') {
      parameters = {};
    }

    let obligatoryParameters: string[] = [];

    if (typeof this.obligatoryParameters === 'object') {
      obligatoryParameters = Object.keys(this.obligatoryParameters);
    }

    if (Object.keys(parameters).length > obligatoryParameters.length) {
      throw new Error('Extraneous parameters provided.');
    }

    return this.urlFragment
      .split('/')
      .map((part) => {
        if (part.indexOf(':') !== 0) {
          return part;
        }

        const parameter = part.substr(1);

        // @ts-ignore
        if (typeof parameters[parameter] === 'undefined') {
          throw new Error(`Parameter '${parameter}' not provided.`);
        }

        // @ts-ignore
        return parameters[parameter];
      }).join('/');
  }
}

/**
 * Possible requests
 */
export type SCRequests =
  SCBookAvailabilityRequest
  | SCBulkRequest
  | SCBulkAddRequest
  | SCBulkDoneRequest
  | SCFeedbackRequest
  | SCIndexRequest
  | SCMultiSearchRequest
  | SCSearchRequest
  | SCThingUpdateRequest;

/**
 * Possible responses
 */
export type SCResponses =
  SCBookAvailabilityResponse
  | SCBulkResponse
  | SCBulkAddResponse
  | SCBulkDoneResponse
  | SCFeedbackResponse
  | SCIndexResponse
  | SCMultiSearchResponse
  | SCSearchResponse
  | SCThingUpdateResponse;
