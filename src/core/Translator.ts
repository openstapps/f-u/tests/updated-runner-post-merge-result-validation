/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCClasses, SCThingsField} from './Classes';
import {SCThing, SCThingType} from './Thing';

import {SCTranslations} from './types/i18n';

import {Defined, OCType} from 'ts-optchain';

/**
 * SCThingTranslator class
 */
export class SCThingTranslator {
  /**
   * Property representing the translators base language.
   * This means every translation is given for this language.
   */
  private baseLanguage: keyof SCTranslations<SCThing>;

  /**
   * Property representing the translators target language
   */
  private language: keyof SCTranslations<SCThing>;

  /**
   * Property provinding a mapping from a SCThingType to its known own meta class.
   */
  private metaClasses: typeof SCClasses;

  /** 
   * @constructor
   * @example
   * // returns translator instance for german
   * new SCThingTranslator('de');
   */
  constructor(language: keyof SCTranslations<SCThing>, baseLanguage?: keyof SCTranslations<SCThing>) {
    this.baseLanguage = baseLanguage ? baseLanguage : 'en';
    this.language = language;
    this.metaClasses = SCClasses;
  }

  /**
   * Get field value translation recursively
   *
   * @param firstObject Top level object that gets passed through the recursion
   * @param data The intermediate object / primitive returned by the Proxys get() method
   * @param keyPath The keypath that (in the end) leads to the translatable property (when added to firstObject)
   * @returns an OCType<T> object allowing for access to translations or a translated value(s)
   */
  private deeptranslate<T, K extends SCThing>(firstObject: K, data?: T, keyPath?: string): OCType<T> {
    const proxy = new Proxy(
      ((defaultValue?: Defined<T>) => (data == null ? defaultValue : data)) as OCType<T>,
      {
        get: (target, key) => {
          const obj: any = target();
          const extendedKeyPath = [keyPath, key.toString()].filter((e) => e != null).join('.');
          let possiblePrimitive = obj[key];
          // check if obj[key] is an array that contains primitive type (arrays in SCThings are not mixing types)
          if (obj[key] instanceof Array && obj[key].length) {
            possiblePrimitive = obj[key][0];
          }
          if (typeof possiblePrimitive === 'string' ||
            typeof possiblePrimitive === 'number' ||
            typeof possiblePrimitive === 'boolean') {
            // returns final translation for primitive data types 
            return this.deeptranslate(firstObject,
                                      this.getFieldValueTranslation(firstObject, extendedKeyPath),
                                      extendedKeyPath);
          }
          // recursion to get more calls to the Proxy handler 'get()' (key path not complete)
          return this.deeptranslate(firstObject, obj[key], extendedKeyPath);
        },
      },
    );
    return proxy;
  }

  /**
   * Applies only known field translations of the given SCThings meta class to an instance
   *
   * @param thingType The type of thing that will be translated
   * @param language The language the thing property values are translated to
   * @returns The thing with all known meta values translated
   */
  private getAllMetaFieldTranslations<T extends SCThing>(thingType: SCThingType,
                                                         language: keyof SCTranslations<T>): object | undefined {
    const fieldTranslations = {};
    const metaClass = this.getMetaClassInstance(thingType);
    if (metaClass === undefined) {
      return undefined;
    }

    // Assigns every property in fieldTranslations to the known base language translation
    if (metaClass.fieldTranslations[this.baseLanguage] !== undefined) {
      Object.keys(metaClass.fieldTranslations[this.baseLanguage]).forEach((key) => {
        (fieldTranslations as any)[key] = metaClass.fieldTranslations[this.baseLanguage][key];
      });
    }

    // Assigns every property in fieldTranslations to the known translation in given language
    if (metaClass.fieldTranslations[language] !== undefined) {
      Object.keys(metaClass.fieldTranslations[language]).forEach((key) => {
        (fieldTranslations as any)[key] = metaClass.fieldTranslations[language][key];
      });
    }
    return fieldTranslations;
  }

  /**
   * Returns meta class needed for translations given a SCThingType
   *
   * @param thingType 
   * @returns An instance of the metaclass
   */
  private getMetaClassInstance(thingType: SCThingType): any {
    if (thingType in this.metaClasses) {
      return new (this.metaClasses as any)[thingType]();
    }
    return undefined;
  }

  /**
   * Returns property value at a certain (key) path of an object.
   * @example
   * // returns value of dish.offers[0].inPlace.categories[1]
   * const dish: SCDish = {...};
   * this.valueFromPath(dish, 'offers[0].inPlace.categories[1]');
   * @param path Key path to evaluate
   * @param obj Object to evaluate the key path upon
   * @param separator Key path seperation element. Defaults to '.'
   * @returns Property value at at key path
   */
  private valueFromPath<T extends SCThing>(path: string, obj: T, separator = '.') {
    path = path.replace(/\[/g, '.');
    path = path.replace(/\]/g, '.');
    path = path.replace(/\.\./g, '.');
    path = path.replace(/\.$/, '');
    const properties = path.split(separator);
    return properties.reduce((prev: any, curr: any) => prev && prev[curr], obj);
  }

  /**
   * Get field value translation
   * @example
   * // returns translation of the property (if available) in the language defined when creating the translator object
   * const dish: SCDish = {...};
   * translator.translate(dish, 'offers[0].inPlace.categories[1]');
   * @param thing SCThing to get value translation for
   * @param field Field to get value translation for (keypath allowed)
   * @returns Translated value(s) or value(s) itself
   */
  public getFieldValueTranslation<T extends SCThing>(thing: T,
                                                     field: SCThingsField): string | string[] {
    let translationPath = 'translations.' + this.language + '.' + field;
    const regexTrimProperties = /.*(?:(\..*)(\[\d+\])|(\.[^\d]*$)|(\..*)(\.[\d]*$))/;

    const pathMatch = field.match(regexTrimProperties);

    // when translation is given in thing 
    let translation = this.valueFromPath(translationPath, thing);
    if (translation) {
      return translation;
    } else if (pathMatch && pathMatch[1] && pathMatch[2] || pathMatch && pathMatch[4] && pathMatch[5]) {
      // accessing iteratable of nested thing
      const keyPath = (pathMatch[1] ? pathMatch[1] : pathMatch[4]) + (pathMatch[2] ? pathMatch[2] : pathMatch[5]);
      const redactedField = field.replace(keyPath, '');

      // when translation is given in nested thing
      translationPath = `${redactedField}.translations.${this.language}${keyPath}`;
      translation = this.valueFromPath(translationPath, thing);
      if (translation) {
        return translation;
      }

      // when translation is given in nested meta thing via iterateable index
      const nestedType = this.valueFromPath(field.replace(keyPath, '.type'), thing) as SCThingType;
      translationPath = `fieldValueTranslations.${this.language}${keyPath}`;
      translation = this.valueFromPath(translationPath.replace(
        /\[(?=[^\[]*$).*|(?=[\d+]*$).*/, '[' + this.valueFromPath(field, thing) + ']'),
        this.getMetaClassInstance(nestedType));
      if (translation) {
        return translation;
      }

    } else if (pathMatch && pathMatch[3]) {
      // accessing meta or instance of nested thing primitive value depth > 0
      const keyPath = pathMatch[3];
      const redactedField = field.replace(pathMatch[3], '');

      // when translation is given in nested thing 
      translationPath = `${redactedField}.translations.${this.language}${keyPath}`;
      if (this.valueFromPath(translationPath, thing)) {
        return this.valueFromPath(translationPath, thing);
      }

      // when translation is given in nested meta thing 
      const nestedType = this.valueFromPath(field.replace(keyPath, '.type'), thing) as SCThingType;
      translationPath = `fieldValueTranslations.${this.language}${keyPath}`;
      translation = this.valueFromPath(translationPath, this.getMetaClassInstance(nestedType));
      if (translation instanceof Object) { // lookup translated keys in meta thing property
        const translations: string[] = [];
        this.valueFromPath(field, thing).forEach((key: string) => {
          translationPath = `fieldValueTranslations.${this.language}${keyPath}.${key}`;
          translations.push(this.valueFromPath(translationPath, this.getMetaClassInstance(nestedType)));
        });
        return translations;
      }
      if (!translation) { // translation not given, return as is
        return this.valueFromPath(field, thing) as string;
      }
      return translation;
    }
    // accessing meta thing primitive value depth = 0
    translationPath = `fieldValueTranslations.${this.language}.${field}`;
    translation = this.valueFromPath(translationPath, this.getMetaClassInstance(thing.type));
    if (translation) {
      if (translation instanceof Object) { // lookup translated keys in meta thing property
        const translations: string[] = [];
        this.valueFromPath(field, thing).forEach((key: string) => {
          translationPath = `fieldValueTranslations.${this.language}.${field}.${key}`;
          translations.push(this.valueFromPath(translationPath, this.getMetaClassInstance(thing.type)));
        });
        return translations;
      }
      return translation;
    }

    // accessing meta thing primitive via iteratable index value depth = 0
    translation = this.valueFromPath(translationPath.replace(
      /\[(?=[^\[]*$).*|(?=[\d+]*$).*/, '[' + this.valueFromPath(field, thing) + ']'),
      this.getMetaClassInstance(thing.type));
    if (translation) {
      return translation;
    }
    // last resort: return as is
    return this.valueFromPath(field, thing) as string;
  }

  /**
   * Get field value translation recursively
   * @example
   * const dish: SCDish = {...};
   * translator.translate(dish).offers[0].inPlace.categories[1]());
   * // or
   * const dishTranslatedAccess = translator.translate(dish);
   * dishTranslatedAccess.offers[0].inPlace.categories[1]();
   * // undoing the OCType<T>
   * const dishAsBefore: SCDish = dishTranslatedAccess()!;
   * @param data Top level object that gets passed through the recursion
   * @returns an OCType<T> object allowing for access to translations or a translated value(s)
   */
  public translate<T extends SCThing>(data?: T): OCType<T> {
    return new Proxy(
      ((defaultValue?: Defined<T>) => (data == null ? defaultValue : data)) as OCType<T>,
      {
        get: (target, key) => {
          const obj: any = target();
          let translatable = obj[key];
          if (obj[key] instanceof Array && obj[key].length) {
            translatable = obj[key][0];
            if (typeof obj[key][0] === 'object' && !obj[key][0].origin) {
              translatable = obj[key][0][Object.keys(obj[key][0])[0]];
            }
          }
          if (typeof translatable === 'string') {
            // retrieve final translation
            return this.deeptranslate(data!, this.getFieldValueTranslation(data!, key.toString()), key.toString());
          }
          // recursion to get more calls to the Proxy handler 'get()' (key path not complete)
          return this.deeptranslate(data!, obj[key], key.toString());
        },
      },
    );
  }

  /**
   * Given a SCThingType this function returns an object with the same basic structure as the corresponding SCThing.
   * All the values will be set to the known translations of the property/key name.
   * @example
   * const translatedMetaDish = translator.translatedPropertyNames<SCCourseOfStudies>(SCThingType.CourseOfStudies);
   * @param language The language the object is translated to
   * @param thingType
   * @returns An object with the properties of the SCThingType where the values are the known property tranlations
   */
  public translatedPropertyNames<T extends SCThing>(thing: T,
                                                    language?: keyof SCTranslations<T>): T | undefined {
    const targetLanguage = (language) ? language : this.language;
    // return {...{}, ...this.getAllMetaFieldTranslations(thing.type, targetLanguage) as T};
    return this.getAllMetaFieldTranslations(thing.type, targetLanguage) as T;
  }
}
