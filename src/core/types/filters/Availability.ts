/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingsField} from '../../Classes';
import {SCISO8601Date} from '../Time';
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments} from './Abstract';

/**
 * An availability filter
 *
 * Filter for documents where it cannot be safely determined that they are not available
 */
export interface SCSearchAvailabilityFilter extends SCSearchAbstractFilter<SCAvailabilityFilterArguments> {
  type: 'availability';
}

/**
 * Arguments for filter instruction by availability
 */
export interface SCAvailabilityFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * Field which marks the start of the availability
   */
  fromField: SCThingsField;

  /**
   * Time to check. Defaults to 'now'
   */
  time?: SCISO8601Date | 'now';

  /**
   * Field which marks the end of the availability
   */
  toField: SCThingsField;
}
