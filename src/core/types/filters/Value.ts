/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingsField} from '../../Classes';
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments} from './Abstract';

/**
 * Filters for documents that match the value on the given field
 */
export interface SCSearchValueFilter extends SCSearchAbstractFilter<SCValueFilterArguments> {
  type: 'value';
}

export interface SCValueFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * Field to filter for a value.
   */
  field: SCThingsField;

  /**
   * Value to filter. Value has to match the field exactly.
   */
  value: string;
}
