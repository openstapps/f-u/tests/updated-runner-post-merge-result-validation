/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSportCoursePriceGroup} from '../../things/DateSeries';
import {SCSearchAbstractSort, SCSearchAbstractSortArguments} from './Abstract';

/**
 * Sort instruction to sort by price
 */
export interface SCPriceSort extends SCSearchAbstractSort<SCPriceSortArguments> {
  type: 'price';
}

/**
 * Additional arguments for sort instruction to sort by price
 */
export interface SCPriceSortArguments extends SCSearchAbstractSortArguments {
  /**
   * University role to sort price for
   */
  universityRole: keyof SCSportCoursePriceGroup;
}
