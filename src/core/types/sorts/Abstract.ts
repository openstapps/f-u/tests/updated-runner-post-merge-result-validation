/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingsField} from '../../Classes';
import {SCMap} from '../Map';
import {SCDistanceSort} from './Distance';
import {SCDucetSort} from './Ducet';
import {SCPriceSort} from './Price';

/**
 * Abstract sort instruction
 */
export interface SCSearchAbstractSort<T extends SCSearchAbstractSortArguments> {
  /**
   * Map of arguments for the sort instruction
   */
  arguments: T;

  /**
   * Direction of the sort instruction: `asc`ending or `desc`ending.
   */
  order: 'asc' | 'desc';

  /**
   * Type of the sort instruction
   */
  type: SCSearchSortType;
}

/**
 * Map of arguments for the sort instruction
 */
export interface SCSearchAbstractSortArguments extends SCMap<any> {
  /**
   * Field to sort by
   */
  field: SCThingsField;
}

/**
 * Type of a sort instruction
 */
export type SCSearchSortType = 'distance' | 'price' | 'ducet';

/**
 * A sort instruction
 */
export type SCSearchSort = SCDistanceSort | SCPriceSort | SCDucetSort;
