/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingWithTranslations} from '../base/ThingWithTranslations';
import {SCBulkResponse} from '../protocol/routes/bulk/BulkResponse';
import {SCMultiSearchResponse} from '../protocol/routes/search/MultiSearchResponse';
import {SCSearchResponse} from '../protocol/routes/search/SearchResponse';
import {SCThing, SCThingType} from '../Thing';

/**
 * Type guard to check if something is a SCThing
 *
 * @param {any} something Something to check
 */
export function isThing(something: any): something is SCThing {
  return (
    typeof something === 'object'
    && typeof something.type === 'string'
    && Object.values(SCThingType).indexOf(something.type) >= 0
  );
}

/**
 * Type guard to check if translations exist
 *
 * @param {SCThing} thing Thing to check
 */
export function isThingWithTranslations(thing: SCThing): thing is SCThingWithTranslations {
  return typeof thing.translations !== 'undefined';
}

/**
 * Type guard to check if something is a bulk response
 *
 * @param {any} something Something to check
 */
export function isBulkResponse(something: any): something is SCBulkResponse {
  return typeof something.expiration === 'string'
    && typeof something.source === 'string'
    && typeof something.state === 'string'
    && typeof something.type === 'string'
    && typeof something.uid === 'string';
}

/**
 * Type guard to check if something is a search response
 *
 * @param {any} something Something to check
 */
export function isSearchResponse(something: any): something is SCSearchResponse {
  return Array.isArray(something.data)
    && Array.isArray(something.facets)
    && typeof something.pagination !== 'undefined'
    && typeof something.pagination.count === 'number'
    && typeof something.pagination.offset === 'number'
    && typeof something.pagination.total === 'number'
    && typeof something.stats !== 'undefined'
    && typeof something.stats.time === 'number';
}

/**
 * Type guard to check if something is a multi search response
 *
 * @param something Something to check
 */
export function isMultiSearchResponse(something: any): something is SCMultiSearchResponse {
  return Object.keys(something).reduce((previousOnesAreSearchResponses, key) => {
    return previousOnesAreSearchResponses && isSearchResponse(something[key]);
  }, true as boolean);
}
