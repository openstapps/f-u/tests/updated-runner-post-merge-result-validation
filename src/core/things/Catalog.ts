/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAcademicTermWithoutReferences} from '../base/AcademicTerm';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * A catalog without references
 */
export interface SCCatalogWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCCatalogCategories,
    SCThingWithCategoriesSpecificValues> {
  /**
   * Level of the catalog (0 for 'root catalog', 1 for its subcatalog, 2 for its subcatalog etc.)
   *
   * Needed for keeping order in catalog inheritance array.
   */
  level: number;

  /**
   * Type of a catalog
   */
  type: SCThingType.Catalog;
}

/**
 * A catalog
 *
 * @validatable
 */
export interface SCCatalog extends SCCatalogWithoutReferences {
  /**
   * Academic term that a catalog belongs to (e.g. semester)
   */
  academicTerm?: SCAcademicTermWithoutReferences;

  /**
   * The direct parent of a catalog
   */
  superCatalog?: SCCatalogWithoutReferences;

  /**
   * An array of catalogs from the 'level 0' (root) catalog to the direct parent
   */
  superCatalogs?: SCCatalogWithoutReferences[];

  /**
   * Type of a catalog
   */
  type: SCThingType.Catalog;
}

  /**
   * Catalog meta data
   */
export class SCCatalogMeta extends SCThingMeta implements SCMetaTranslations<SCCatalog> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCCatalogCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCCatalogCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCCatalogCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      type: 'Verzeichnis',
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCCatalogCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.Catalog,
    },
  };
}

/**
 * Categories of catalogs
 */
export type SCCatalogCategories = 'university events';
