/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSaveableThing, SCSaveableThingWithoutReferences} from '../base/SaveableThing';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCAcademicEventWithoutReferences} from './AcademicEvent';
import {SCArticleWithoutReferences} from './Article';
import {SCBookWithoutReferences} from './Book';
import {SCBuildingWithoutReferences} from './Building';
import {SCPersonWithoutReferences} from './Person';
import {SCPointOfInterestWithoutReferences} from './PointOfInterest';
import {SCRoomWithoutReferences} from './Room';
import {SCSportCourseWithoutReferences} from './SportCourse';
import {SCToDoWithoutReferences} from './ToDo';

/**
 * Types that can be made a favorite (added as a favorite)
 */
export type SCFavoriteDataTypes = SCAcademicEventWithoutReferences
  | SCArticleWithoutReferences
  | SCBookWithoutReferences
  | SCBuildingWithoutReferences
  | SCPersonWithoutReferences
  | SCPointOfInterestWithoutReferences
  | SCRoomWithoutReferences
  | SCSportCourseWithoutReferences
  | SCToDoWithoutReferences;

/**
 * A favorite without references
 */
export interface SCFavoriteWithoutReferences extends SCSaveableThingWithoutReferences {

}

/**
 * A favorite
 *
 * @validatable
 */
export interface SCFavorite extends SCSaveableThing<SCFavoriteDataTypes> {
  /**
   * Type of a favorite
   */
  type: SCThingType.Favorite;
}

/**
 * Meta information about a favorite
 */
export class SCFavoriteMeta extends SCThingMeta {
}
