/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAcademicDegree, SCAcademicDegreeMeta} from '../base/AcademicDegree';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from '../base/ThingThatCanBeOffered';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCLanguage, SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCDateSeriesWithoutReferences} from './DateSeries';
import {SCOrganization} from './Organization';

/**
 * A course of studies without references
 */
export interface SCCourseOfStudiesWithoutReferences extends SCAcademicDegree,
  SCThingThatCanBeOfferedWithoutReferences {
  /**
   * The main language in which the course of studies
   * is beeing offered
   */
  mainLanguage: SCLanguage;

  /**
   * Actual major of the course of studies (eg. physics)
   */
  major: string;

  /**
   * The modes the course of studies is offered in
   */
  mode: SCCourseOfStudiesMode;

  /**
   * The time modes the course of studies is offered in
   */
  timeMode: SCCourseOfStudiesTimeMode;

  /**
   * Translated fields of a dish
   */
  translations?: SCTranslations<SCCourseOfStudiesTranslatableProperties>;

  /**
   * Type of the course of studies
   */
  type: SCThingType.CourseOfStudies;
}

/**
 * A course of studies
 *
 * @validatable
 */
export interface SCCourseOfStudies extends SCCourseOfStudiesWithoutReferences,
  SCThingThatCanBeOffered<SCAcademicPriceGroup> {
  /**
   * The department that manages the course of studies
   */
  department: SCOrganization;

  /**
   * The secretary that administers requests and
   * questions concerning the course of studies
   */
  secretary: SCOrganization;

  /**
   * Dates at which the course of studies is planned to start
   */
  startDates?: SCDateSeriesWithoutReferences[];

  /**
   * Translated fields of a dish
   */
  translations?: SCTranslations<SCCourseOfStudiesTranslatableProperties>;

  /**
   * Type of the course of studies
   */
  type: SCThingType.CourseOfStudies;
}

export interface SCCourseOfStudiesTranslatableProperties
  extends SCThingThatCanBeOfferedTranslatableProperties {
}

/**
 * Meta information about a course of studies
 */
export class SCCourseOfStudiesMeta extends SCThingMeta implements SCMetaTranslations<SCCourseOfStudies> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...SCAcademicDegreeMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ...SCAcademicDegreeMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...SCAcademicDegreeMeta.getInstance().fieldValueTranslations.de,
      modes: {
        combination: 'Kombinationsstudiengang',
        dual: 'Dualer Studiengang',
        fulltime: 'Vollzeitstudiengang',
        parttime: 'Teilzeitstudiengang',
      },
      type: 'Studiengang',
    },
    en: {
      ...SCAcademicDegreeMeta.getInstance().fieldValueTranslations.en,
      academicDegree: 'Hochschulabschluss',
      department: 'Fachbereich',
      major: 'Studienfach',
      modes: 'Studiengangsarten',
      secretary: 'Sekretariat',
      type: SCThingType.CourseOfStudies,
    },
  };
}

/**
 * Types of (german) course of studies modes
 */
export type SCCourseOfStudiesMode = 'combination'
  | 'dual'
  | 'double-degree'
  | 'standard' ;

/**
 * Types of (german) course of studies time modes
 */
export type SCCourseOfStudiesTimeMode = 'fulltime'
  | 'parttime' ;
