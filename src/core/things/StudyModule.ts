/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAcademicPriceGroup,
        SCThingThatCanBeOffered,
        SCThingThatCanBeOfferedTranslatableProperties} from '../base/ThingThatCanBeOffered';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCLanguage, SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCMap} from '../types/Map';
import {SCAcademicEventWithoutReferences} from './AcademicEvent';
import {SCOrganizationWithoutReferences} from './Organization';
import {SCPersonWithoutReferences} from './Person';

/**
 * A study module without references
 */
export interface SCStudyModuleWithoutReferences extends
                 SCThingThatCanBeOffered<SCAcademicPriceGroup> {

  /**
   * ECTS points (European Credit Transfer System)
   */
  ects: number;

  /**
   * The language in which the study module is offered
   */
  language: SCLanguage;

  /**
   * Majors that this study module is meant for
   */
  majors: string[];

  /**
   * Represents the modules necessity for each given major (of the major property)
   */
  necessity: SCMap<SCStudyModuleNecessity>;

  /**
   * Translated fields of a study module
   */
  translations?: SCTranslations<SCStudyModuleTranslatableProperties>;

  /**
   * Type of the study module
   */
  type: SCThingType.StudyModule;
}

/**
 * A study module
 * 
 * @validatable
 */
export interface SCStudyModule extends SCStudyModuleWithoutReferences {
  /**
   * Academic events that make up a study module
   */
  academicEvents: SCAcademicEventWithoutReferences[];

  /**
   * The faculty that manages and curates the study module
   */
  faculty: SCOrganizationWithoutReferences;

  /**
   * Study modules needed for each others fulfillment
   */
  partnerModules?: SCStudyModuleWithoutReferences[];

  /**
   * Study modules required beforehand
   */
  requiredModules?: SCStudyModuleWithoutReferences[];

  /**
   * The secretary that administers requests and 
   * questions concerning the study module by eg. students
   */
  secretary: SCOrganizationWithoutReferences | SCPersonWithoutReferences;
}

export interface SCStudyModuleTranslatableProperties
       extends SCThingThatCanBeOfferedTranslatableProperties {
  /**
   * Translations of the majors that this study module is meant for
   */
  majors?: string[];

  /**
   * Translations of the modules necessity for each given major (of the major property)
   */
  necessity: SCMap<SCStudyModuleNecessity>;
}

/** 
 * Represents a modules necessity (in a major) as it may be required, optional or 
 * is in a pool of n optional modules were m out of them have to be taken/completed.
 * Hence the elective option.
 */
export enum SCStudyModuleNecessity {
  Required = 'required',
  Elective = 'elective',
  Optional = 'optional',
}

/**
 * Study module meta data
 */
export class SCStudyModuleMeta extends SCThingMeta implements SCMetaTranslations<SCStudyModule> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
      academicEvents: 'Veranstaltungen',
      ects: 'ECTS-Punkte',
      faculty: 'Fachbereich',
      language: 'Unterrichtssprache',
      majors: 'Fachrichtungen',
      necessity: 'Erforderlichkeit',
      partnerModules: 'Partnermodule',
      requiredModules: 'Benötigte Module',
      secretary: 'Sekretariat',
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
      academicEvents: 'academic events',
      ects: 'ECTS points',
      faculty: 'faculty',
      language: 'teaching language',
      majors: 'majors',
      necessity: 'necessity',
      partnerModules: 'partner modules',
      requiredModules: 'required modules',
      secretary: 'secretary',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
      necessity: {
        'elective' : 'Wahlfach',
        'optional' : 'optional',
        'required' : 'benötigt',
      },
      type: 'Studiengangmodul',
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.StudyModule,
    },
  };
}
