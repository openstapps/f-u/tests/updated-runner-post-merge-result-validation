/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';

/**
 * A "to do" without references
 */
export interface SCToDoWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<string, SCThingWithCategoriesSpecificValues> {
  /**
   * Whether or not the "to do" is already done
   */
  done: boolean;

  /**
   * A date when the "to do" is due
   */
  dueDate?: SCISO8601Date;

  /**
   * Priority of the "to do"
   */
  priority: SCToDoPriority;

  /**
   * Type of the "to do"
   */
  type: SCThingType.ToDo;
}

/**
 * A "to do"
 *
 * @validatable
 */
export interface SCToDo extends SCToDoWithoutReferences {
}

/**
 * A priority of a "to do"
 */
export enum SCToDoPriority {
  LOW = 0,
  NORMAL = 2,
  HIGH = 5,
}

/**
 * Meta information about todo
 */
export class SCToDoMeta extends SCThingMeta implements SCMetaTranslations<SCToDo> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<string,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<string,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<string,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      type: 'ToDo',
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<string,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.ToDo,
    },
  };
}
