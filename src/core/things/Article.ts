/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from '../base/CreativeWork';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

/**
 * An article without references
 */
export interface SCArticleWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCArticleCategories,
      SCThingWithCategoriesSpecificValues> {
  /**
   * Article itself as markdown
   */
  articleBody: string;

  /**
   * Translated fields of an article
   */
  translations?: SCTranslations<SCArticleTranslatableProperties>;

  /**
   * Type of an article
   */
  type: SCThingType.Article;
}

/**
 * An article
 *
 * @validatable
 */
export interface SCArticle extends SCCreativeWork, SCArticleWithoutReferences {
  /**
   * Translated fields of an article
   */
  translations?: SCTranslations<SCArticleTranslatableProperties>;

  /**
   * Type of an article
   */
  type: SCThingType.Article;
}

/**
 * Categories of articles
 */
export type SCArticleCategories = 'unipedia';

/**
 * Translatable properties of creative works
 */
export interface SCArticleTranslatableProperties
  extends SCThingWithCategoriesTranslatableProperties, SCCreativeWorkTranslatableProperties {
  /**
   * Translation of the article itself as markdown
   */
  articleBody?: string[];
}

/**
 * Meta information about an article
 */
export class SCArticleMeta extends SCThingMeta implements SCMetaTranslations<SCArticle> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCArticleCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCArticleCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCArticleCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      type: 'Artikel',
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCArticleCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.Article,
    },
  };
}
