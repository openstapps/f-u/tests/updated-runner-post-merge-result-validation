/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import * as jsonpatch from 'json-patch';
import {SCThingsWithoutDiff} from '../Classes';
import {SCThing, SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';

/**
 * A diff without references
 */
export interface SCDiffWithoutReferences extends SCThing {
  /**
   * Action of the diff
   */
  action: 'changed' | 'removed';

  /**
   * Diff patch. Only when action === 'changed'
   */
  changes?: jsonpatch.OpPatch[];

  /**
   * Creation date of the diff.
   */
  dateCreated: SCISO8601Date;

  /**
   * Type of a diff
   */
  type: SCThingType.Diff;
}

/**
 * A diff
 *
 * @validatable
 */
export interface SCDiff extends SCDiffWithoutReferences {
  /**
   * Original object the diff was generated on
   */
  object: SCThingsWithoutDiff;

  /**
   * Type of a diff
   */
  type: SCThingType.Diff;
}

/**
 * Meta information about a diff
 */
export class SCDiffMeta extends SCThingMeta implements SCMetaTranslations<SCDiff> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
      action: 'Aktion',
      changes: 'Änderungen',
      dateCreated: 'Erstellungsdatum',
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
      type: 'Unterschied',
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Diff,
    },
  };
}
