/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from '../base/ThingThatCanBeOffered';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

/**
 * A dish without references
 */
export interface SCDishWithoutReferences extends SCThingThatCanBeOfferedWithoutReferences,
  SCThingWithCategoriesWithoutReferences<SCDishCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Additives of the dish
   */
  additives?: string[];

  /**
   * Category of the dish
   */
  categories: SCDishCategories[];

  /**
   * Characteristics of the dish
   */
  characteristics?: SCDishCharacteristic[];

  /**
   * Nutrition information (calories and nutrients with amounts)
   */
  nutrition?: SCNutritionInformation;

  /**
   * Translated fields of a dish
   */
  translations?: SCTranslations<SCDishTranslatableProperties>;

  /**
   * Type of a dish
   */
  type: SCThingType.Dish;
}

/**
 * A dish
 *
 * @validatable
 */
export interface SCDish extends SCDishWithoutReferences,
  SCThingThatCanBeOffered<SCAcademicPriceGroup> {
  /**
   * Dishes ("Beilagen") that are served with the dish (if only certain supplement dishes can be taken with a dish)
   */
  dishAddOns?: SCDishWithoutReferences[];

  /**
   * Translated fields of a dish
   */
  translations?: SCTranslations<SCDishTranslatableProperties>;

  /**
   * Type of a dish
   */
  type: SCThingType.Dish;
}

export interface SCDishTranslatableProperties
  extends SCThingWithCategoriesTranslatableProperties, SCThingThatCanBeOfferedTranslatableProperties {
  /**
   * Characteristics of the dish
   */
  characteristics?: SCDishCharacteristic[];
}

/**
 * Composition of properties of a food characteristic
 */
export interface SCDishCharacteristic {
  /**
   * URL to the image of the characteristic
   */
  image?: string;

  /**
   * Name of the characteristic
   */
  name: string;
}

/**
 * A list of categories for dishes
 */
export type SCDishCategories =
  | 'appetizer'
  | 'salad'
  | 'main dish'
  | 'dessert'
  | 'soup'
  | 'side dish';

/**
 * Type definition for SCNutritionInformation
 *
 * @see https://schema.org/NutritionInformation
 */
export interface SCNutritionInformation {
  /**
   * Number of calories contained (in kcal)
   */
  calories?: number;

  /**
   * Content of carbohydrates (in grams)
   */
  carbohydrateContent?: number;

  /**
   * Content of fat (in grams)
   */
  fatContent?: number;

  /**
   * Content of proteins (in grams)
   */
  proteinContent?: number;

  /**
   * Content of salt (in grams)
   */
  saltContent?: number;

  /**
   * Content of saturated fat (in grams)
   */
  saturatedFatContent?: number;

  /**
   * Content of sugar (in grams)
   */
  sugarContent?: number;
}

/**
 * Meta information about a dish
 */
export class SCDishMeta extends SCThingMeta implements SCMetaTranslations<SCDish> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCDishCategories,
        SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
    },
    en: {
      ...SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCDishCategories,
        SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCDishCategories,
        SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      categories: {
        appetizer: 'Vorspeise',
        dessert: 'Nachtisch',
        'main dish': 'Hauptgericht',
        salad: 'Salat',
        'side dish': 'Beilage',
        soup: 'Suppe',
      },
      type: 'Essen',
    },
    en: {
      ...SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCDishCategories,
        SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.Dish,
    },
  };
}
