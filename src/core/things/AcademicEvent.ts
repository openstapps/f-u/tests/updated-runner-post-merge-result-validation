/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCEvent, SCEventMeta, SCEventWithoutReferences} from '../base/Event';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

/**
 * An academic event without references
 */
export interface SCAcademicEventWithoutReferences
  extends SCEventWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCAcademicEventCategories,
      SCThingWithCategoriesSpecificValues> {
  /**
   * Majors of the academic event that this event belongs to
   */
  majors?: string[];

  /**
   * Original unmapped category from the source of the academic event
   */
  originalCategory?: string;

  /**
   * Translated fields of an academic event
   */
  translations?: SCTranslations<SCAcademicEventTranslatableProperties>;

  /**
   * Type of an academic event
   */
  type: SCThingType.AcademicEvent;
}

/**
 * An academic event
 *
 * @validatable
 */
export interface SCAcademicEvent extends SCEvent, SCAcademicEventWithoutReferences {
  /**
   * Translated fields of an academic event
   */
  translations?: SCTranslations<SCAcademicEventTranslatableProperties>;

  /**
   * Type of an academic event
   */
  type: SCThingType.AcademicEvent;
}

/**
 * Categories of academic events
 */
export type SCAcademicEventCategories =
  'lecture'
  | 'seminar'
  | 'integrated course'
  | 'written exam'
  | 'tutorial'
  | 'project'
  | 'colloquium'
  | 'practicum'
  | 'introductory class'
  | 'course'
  | 'practicum introduction'
  | 'excursion'
  | 'special';

/**
 * Translatable properties of an academic event
 */
export interface SCAcademicEventTranslatableProperties
  extends SCThingWithCategoriesTranslatableProperties {
  /**
   * Translations of the majors of the academic event that this event belongs to
   */
  majors?: string[];

  /**
   * Translation of the original unmapped category from the source of the academic event
   */
  originalCategory?: string;
}

/**
 * Meta information about academic events
 */
export class SCAcademicEventMeta extends SCThingMeta implements SCMetaTranslations<SCAcademicEvent> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCEventMeta.getInstance().fieldTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCAcademicEventCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
    },
    en: {
      ... SCEventMeta.getInstance().fieldTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCAcademicEventCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCEventMeta.getInstance().fieldValueTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCAcademicEventCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      type: 'akademische Veranstaltung',
    },
    en: {
      ... SCEventMeta.getInstance().fieldValueTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCAcademicEventCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.AcademicEvent,
    },
  };
}
