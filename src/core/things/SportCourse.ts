/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCEvent, SCEventMeta, SCEventWithoutReferences} from '../base/Event';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * A sport course without references
 */
export interface SCSportCourseWithoutReferences extends SCEventWithoutReferences {
  /**
   * Type of a sport course
   */
  type: SCThingType.SportCourse;
}

/**
 * A sport course
 *
 * @validatable
 */
export interface SCSportCourse extends SCEvent, SCSportCourseWithoutReferences {
  /**
   * Type of a sport course
   */
  type: SCThingType.SportCourse;
}

/**
 * Meta information about a sport course
 */
export class SCSportCourseMeta extends SCThingMeta implements SCMetaTranslations<SCSportCourse> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
      ... SCEventMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
      ... SCEventMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
      ... SCEventMeta.getInstance().fieldValueTranslations.de,
      type: 'Sportkurs',
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
      ... SCEventMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.SportCourse,
    },
  };
}
