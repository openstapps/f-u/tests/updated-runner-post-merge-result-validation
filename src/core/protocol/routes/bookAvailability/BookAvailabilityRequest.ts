/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {SCUuid} from '../../../types/UUID';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCNotFoundErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../errors/ErrorResponse';

/**
 * Request to check the availability of books
 * @validatable
 */
export type SCBookAvailabilityRequest = SCBookAvailabilityRequestByIsbn | SCBookAvailabilityRequestByUuid;

/**
 * Availability request by ISBN
 */
export interface SCBookAvailabilityRequestByIsbn {
  /**
   * ISBN of the book to check availability for
   */
  isbn: string;
}

/**
 * Availability request by UUID
 */
export interface SCBookAvailabilityRequestByUuid {
  /**
   * UID of the book to check availability for
   */
  uid: SCUuid;
}

/**
 * Route for book availability
 *
 * This checks if a book is available in a library.
 *
 * **Example**:
 *
 * `POST https://example.com/bookAvailability`
 *
 * ```json
 * {
 *   "isbn": "978-3-16-148410-0"
 * }
 * ```
 */
export class SCBookAvailabilityRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCNotFoundErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCBookAvailabilityRequest';
    this.responseBodyName = 'SCBookAvailabilityResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/bookAvailability';
  }
}
