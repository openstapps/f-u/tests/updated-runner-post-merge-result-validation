/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAppConfiguration} from '../../../types/config/App';
import {SCBackendConfiguration} from '../../../types/config/Backend';

/**
 * A response to an index request
 *
 * @validatable
 */
export interface SCIndexResponse {
  app: SCAppConfiguration;

  backend: SCBackendConfiguration;
}
