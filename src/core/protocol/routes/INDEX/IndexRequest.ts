/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../errors/ErrorResponse';

/**
 * Index request
 *
 * @validatable
 */
export interface SCIndexRequest {
}

/**
 * Route to request meta information about the deployment
 */
export class SCIndexRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCIndexRequest';
    this.responseBodyName = 'SCIndexResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/';
  }
}
